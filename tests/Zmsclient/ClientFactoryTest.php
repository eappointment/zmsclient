<?php
/**
 * @copyright BerlinOnline Stadtportal GmbH & Co. KG
 **/

declare(strict_types=1);

namespace BO\Zmsclient\Tests;

use BO\Zmsclient\ClientFactory;
use BO\Zmsclient\Http;
use PHPUnit\Framework\TestCase;

class ClientFactoryTest extends TestCase
{
    public function testCreateClient()
    {
        $httpClient = ClientFactory::createClient(ClientFactory::TYPE_SYNCHRONOUS_HTTP, ZMS_API_URL);

        self::assertInstanceOf(Http::class, $httpClient);
    }
}
