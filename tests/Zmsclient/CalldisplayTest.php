<?php

namespace BO\Zmsclient\Tests;

class CalldisplayTest extends Base
{
    const HASH_TEST = '0058pfv918e8ipmbadj05sm1e7';

    /**
     * @runInSeparateProcess
     */
    public function testBasic()
    {
        $request = new \BO\Zmsclient\Psr7\Request('GET', '/');
        $this->assertFalse(\BO\Zmsclient\Calldisplay::getCookieValue());
        \BO\Zmsclient\Calldisplay::setCookieValue(self::HASH_TEST, $request);
        $this->assertEquals(self::HASH_TEST, \BO\Zmsclient\Calldisplay::getCookieValue());
    }
}
