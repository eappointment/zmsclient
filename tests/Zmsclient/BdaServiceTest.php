<?php

namespace BO\Zmsclient\Tests;

use BO\Zmsclient\BdaService;
use BO\Zmsentities\Process;
use PHPUnit\Framework\TestCase;

class BdaServiceTest extends TestCase
{
    public const BASE_URL = 'https://www.example.com';

    public const EAPP_BDA_URL = 'eapp-bda://example.com/?parameter1=value1&parameter2=value2';
    public const TENANT_ID = '123456789';
    public const POSITIVE_LIST = ['example.com'];
    public const MESSAGE_TYPE_CHANGE = "bda:qtermin:verschiebung";
    public const MESSAGE_TYPE_DELETE = "bda:qtermin:stornierung";

    public function testConstructor()
    {
        $bdaService = new BdaService(self::BASE_URL, self::TENANT_ID, self::POSITIVE_LIST);
        self::assertTrue($bdaService->getBaseUrl() === self::BASE_URL);
        self::assertTrue($bdaService->getTenantId() === self::TENANT_ID);
        self::assertTrue($bdaService->getPositiveList() === self::POSITIVE_LIST);
    }

    /* does not work anymore with post request to example.com -> mock it
    public function testSendRequest()
    {
        $bdaService = new BdaService(self::BASE_URL, self::TENANT_ID, self::POSITIVE_LIST);
        $process = (new Process())->getExample();
        $response = $bdaService->sendRequest(self::EAPP_BDA_URL, self::MESSAGE_TYPE_CHANGE, $process);
        self::assertStringContainsString('This domain is for use in illustrative examples in documents', $response);
    }
    */

    public function testGetPayloadOnChange()
    {
        $bdaService = new BdaService(self::BASE_URL, self::TENANT_ID, self::POSITIVE_LIST);
        $process = (new Process())->getExample();
        $process->processArchiveId = "12345678910111213141516";
        $payload = $bdaService->getPayload(self::EAPP_BDA_URL, self::MESSAGE_TYPE_CHANGE, $process);
        self::assertStringContainsString('"messageName":"bda:qtermin:verschiebung"', $payload);
        self::assertStringContainsString('"businessKey":"12345678910111213141516","tenantId":"123456789"', $payload);
        self::assertStringContainsString('"nachrichtZeitstempel":1447931596', $payload);
        self::assertStringContainsString('"nachrichtId":"12345678910111213141516"', $payload);
        self::assertStringContainsString('"terminIdZms":123456', $payload);
        self::assertStringContainsString(
            '"terminLink":"\/terminvereinbarung\/admin\/process\/archive\/12345678910111213141516\/"',
            $payload
        );
        self::assertStringContainsString('"terminDatumUhrzeitNeu":"2015-11-18 18:52:51"', $payload);
        self::assertStringContainsString('"standortIdNeu":123', $payload);
        self::assertStringContainsString(',"standortBezeichnungNeu":"Flughafen Sch\u00f6nefeld, Landebahn"', $payload);
        self::assertStringContainsString('"parameter1":"value1","parameter2":"value2"', $payload);
    }

    public function testGetPayloadOnDelete()
    {
        $bdaService = new BdaService(self::BASE_URL, self::TENANT_ID, self::POSITIVE_LIST);
        $process = (new Process())->getExample();
        $process->processArchiveId = "12345678910111213141516";
        $payload = $bdaService->getPayload(self::EAPP_BDA_URL, self::MESSAGE_TYPE_DELETE, $process);
        self::assertStringContainsString('"messageName":"bda:qtermin:stornierung"', $payload);
        self::assertStringContainsString('"businessKey":"12345678910111213141516","tenantId":"123456789"', $payload);
        self::assertStringContainsString('"nachrichtZeitstempel":1447931596', $payload);
        self::assertStringContainsString('"nachrichtId":"12345678910111213141516"', $payload);
        self::assertStringContainsString('"terminIdZms":123456', $payload);
        self::assertStringContainsString(
            '"terminLink":"\/terminvereinbarung\/admin\/process\/archive\/12345678910111213141516\/"',
            $payload
        );
        self::assertStringContainsString('"terminStornoGrund":"abgesagt"', $payload);
        self::assertStringContainsString('"parameter1":"value1","parameter2":"value2"', $payload);
    }

    public function testBadRequest()
    {
        self::expectException('BO\Zmsclient\Exception\BadRequest');
        $bdaService = new BdaService(self::BASE_URL, self::TENANT_ID, self::POSITIVE_LIST);
        $process = (new Process())->getExample();
        $bdaService->sendRequest('https://example.de', self::MESSAGE_TYPE_CHANGE, $process);
    }
}
