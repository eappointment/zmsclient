<?php
/**
 * @copyright BerlinOnline Stadtportal GmbH & Co. KG
 **/

declare(strict_types=1);

namespace BO\Zmsclient;

class ClientFactory
{
    public const TYPE_SYNCHRONOUS_HTTP  = 'zmsHttp';

    public static function createClient(string $type, string $baseUrl)
    {
        switch ($type) {
            case self::TYPE_SYNCHRONOUS_HTTP:
                return new Http($baseUrl);
            default:
                throw new \InvalidArgumentException('The requested client type is not supported/implemented');
        }
    }
}
