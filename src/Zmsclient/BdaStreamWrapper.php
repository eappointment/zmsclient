<?php

/**
 * @copyright BerlinOnline Stadtportal GmbH & Co. KG
 **/

declare(strict_types=1);

namespace BO\Zmsclient;

use Fig\Http\Message\StatusCodeInterface;

/**
 * @SuppressWarnings (CamelCaseMethodName)
 * @SuppressWarnings (UnusedFormalParameter)
 */
// @codingStandardsIgnoreStart
class BdaStreamWrapper
{
    private $curlHandler;
    private $httpStatusCode;
    private $response;
    private $position;

    /**
     * @param string      $path
     * @param string      $mode
     * @param int         $options
     * @param string|null $opened_path
     *
     * @return bool
     * @suppressWarnings(unused)
     */
    public function stream_open(string $path, string $mode, int $options, ?string &$opened_path): bool
    {
        $contextOptions = stream_context_get_options($this->context);
        if (!isset($contextOptions['http'])) {
            return false;
        }
        $httpOptions = $contextOptions['http'];

        $postData = $httpOptions['content'] ?? '';
        $headers = $httpOptions['header'] ?? '';
        $baseUrl = $httpOptions['baseUrl'] ?? '';
        $basePath = $httpOptions['basePath'] ?? '';

        $this->curlHandler = curl_init();
        curl_setopt($this->curlHandler, CURLOPT_URL, $baseUrl . $basePath);
        curl_setopt($this->curlHandler, CURLOPT_POST, 1);
        curl_setopt($this->curlHandler, CURLOPT_POSTFIELDS, $postData);
        curl_setopt($this->curlHandler, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($this->curlHandler, CURLOPT_HEADER, true);

        curl_setopt($this->curlHandler, CURLOPT_HEADERFUNCTION, function ($curlHandler, $header) {
            $length = strlen($header);
            if (preg_match('/^HTTP\/\d(?:\.\d)? (\d+)/', $header, $matches)) {
                $this->httpStatusCode = intval($matches[1]);
            }
            return $length;
        });

        if (!empty($headers)) {
            curl_setopt($this->curlHandler, CURLOPT_HTTPHEADER, explode("\r\n", $headers));
        }

        $this->response = curl_exec($this->curlHandler);
        if ($this->response === false || $this->httpStatusCode !== StatusCodeInterface::STATUS_OK) {
            return false;
        }
        $this->position = 0;
        return  true;
    }

    public function stream_read($count)
    {
        $data = substr($this->response, $this->position, $count);
        $this->position += strlen($data);
        return $data;
    }

    public function stream_eof()
    {
        return $this->position >= strlen($this->response);
    }

    public function stream_close(): void
    {
        curl_close($this->curlHandler);
    }

    public function stream_stat(): array
    {
        return [];
    }

    public function getHttpStatusCode()
    {
        return $this->httpStatusCode;
    }
}
// @codingStandardsIgnoreEnd
