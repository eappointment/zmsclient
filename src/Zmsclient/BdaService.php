<?php

/**
 * @copyright BerlinOnline Stadtportal GmbH & Co. KG
 **/

declare(strict_types=1);

namespace BO\Zmsclient;

use BO\Zmsclient\Exception\BadRequest as BadRequestException;
use BO\Zmsentities\Process as ProcessEntity;

class BdaService
{
    protected const METHOD = "POST";
    protected const CONTENT_TYPE = "application/json";
    protected const MESSAGE_TYPE_CHANGE = "bda:qtermin:verschiebung";
    protected const MESSAGE_TYPE_DELETE = "bda:qtermin:stornierung";

    /** @var string */
    private $baseUrl;

    /** @var string */
    private $tenantId;

    /** @var array */
    private $positiveList;

    private $httpStatusCode;

    public function __construct(string $baseUrl, string $tenantId, array $positiveList = [])
    {
        $this->baseUrl = $baseUrl;
        $this->tenantId = $tenantId;
        $this->positiveList = $positiveList;
    }

    /**
     * @throws BadRequestException
     */
    public function sendRequest(string $url, string $messageName, ProcessEntity $processEntity): string
    {
        $streamWrapper = new BdaStreamWrapper();
        stream_wrapper_register("eapp-bda", get_class($streamWrapper));
        $this->testUrl($url);

        $parsedBaseUrl = parse_url($this->baseUrl);
        $jsonPayload = $this->getPayload($url, $messageName, $processEntity);
        $options = [
            'http' => [
                'header'  => "Content-Type: " . self::CONTENT_TYPE . "\r\n",
                'method'  => self::METHOD,
                'content' => $jsonPayload,
                'baseUrl' => $parsedBaseUrl['scheme'] . '://' . $parsedBaseUrl['host'],
                'basePath' => $parsedBaseUrl['path'] ?? '/',
            ],
        ];
        $result = $this->readContent($url, $options);
        $this->httpStatusCode = $streamWrapper->getHttpStatusCode();
        stream_wrapper_unregister("eapp-bda");
        if ($result === false) {
            throw new BadRequestException();
        }
        return $result;
    }

    protected function readContent(string $url, array $options)
    {
        $context  = stream_context_create($options);
        return file_get_contents($url, false, $context);
    }

    public function getBaseUrl(): string
    {
        return $this->baseUrl;
    }

    public function getTenantId(): string
    {
        return $this->tenantId;
    }

    public function getPositiveList(): array
    {
        return $this->positiveList;
    }

    public function getHttpStatusCode(): int
    {
        return $this->httpStatusCode;
    }

    public function getPayload(string $url, string $messageName, ProcessEntity $processEntity): string
    {
        $parsedUrl = parse_url($url);
        if (!isset($parsedUrl['query'])) {
            $parsedUrl['query'] = '';
        }
        parse_str($parsedUrl['query'], $queryParams);

        $processVariables = [
            'nachrichtZeitstempel' => $processEntity->lastChange,
            'nachrichtId' => $processEntity->processArchiveId,
            'terminIdZms' => $processEntity->getId(),
            'terminLink' => '/terminvereinbarung/admin/process/archive/' .
                $processEntity->processArchiveId . '/'
        ];

        if ($messageName == self::MESSAGE_TYPE_CHANGE) {
            $processVariables['terminDatumUhrzeitNeu'] = $processEntity
                ->getFirstAppointment()
                ->getStartTime()
                ->format('Y-m-d H:i:s');
            $processVariables['standortIdNeu'] = $processEntity->getScopeId();
            $processVariables['standortBezeichnungNeu'] = $processEntity->getCurrentScope()->getName();
        }
        if ($messageName == self::MESSAGE_TYPE_DELETE) {
            $processVariables['terminStornoGrund'] = 'abgesagt';
        }

        foreach ($queryParams as $key => $value) {
            if (in_array($key, ['businessKey', 'messageName', 'tenantId'])) {
                continue;
            }
            $decodedValue = urldecode($value);
            $processVariables[$key] = $decodedValue;
        }

        $payload = [
            'messageName' => $messageName,
            'businessKey' => $processEntity->processArchiveId,
            'tenantId' => $this->tenantId,
            'processVariables' => $processVariables,
            'resultEnabled' => true
        ];

        return json_encode($payload);
    }

    /**
     * @throws BadRequestException
     */
    private function testUrl(string $url): void
    {
        $domain = parse_url($url, PHP_URL_HOST);
        if (!in_array($domain, $this->positiveList)) {
            throw new BadRequestException();
        }
    }
}
