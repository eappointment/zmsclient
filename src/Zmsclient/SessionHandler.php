<?php
namespace BO\Zmsclient;

/**
 * Session handler for mysql
 */
class SessionHandler implements \SessionHandlerInterface
{
    public $sessionName;

    public $savePath;

    public $params = [];

    /**
     * Adds a parameter "sync" on reading the session from the API
     * Use a value of 1 to enable synchronous reads
     * if a former session write happened during a redirect
     */
    public static $useSyncFlag = 0;

    protected static $lastInstance = null;

    /**
     * @var \BO\Zmsclient\Http $http
     *
     */
    protected $http = null;


    public function __construct(Http $http)
    {
        $this->setHttpHandler($http);
        static::$lastInstance = $this;
    }

    public static function getLastInstance()
    {
        return static::$lastInstance;
    }

    public function setHttpHandler(Http $http)
    {
        $this->http = $http;
    }

    /**
     * @SuppressWarnings(UnusedFormalParameter)
     */
    public function open($savePath, $name): bool
    {
        $this->savePath = $savePath;
        $this->sessionName = $name;
        return true;
    }

    public function close(): bool
    {
        return true;
    }

    public function setParams($params = [])
    {
        $this->params = $params;
    }

    public function read($sessionId): string
    {
        $this->params['sync'] = static::$useSyncFlag;
        /** @var \BO\Zmsentities\Session $session */
        try {
            $session = $this->http->readGetResult(
                '/session/' . $this->sessionName . '/' . $sessionId . '/',
                $this->params
            )
            ->getEntity();
        } catch (Exception\ApiFailed $exception) {
            throw $exception;
        } catch (Exception $exception) {
            if ($exception->getCode() == 404) {
                $session = false;
            } else {
                throw $exception;
            }
        }
        if (isset($this->params['oidc']) && 1 == $this->params['oidc'] && $session) {
            $session = $session->withOidcDataOnly();
        }
        return ($session && $session->offsetExists('content')) ? serialize($session->getContent()) : '';
    }

    public function write($sessionId, $sessionData): bool
    {
        $entity = new \BO\Zmsentities\Session();
        $entity->id = $sessionId;
        $entity->name = $this->sessionName;
        $entity->content = unserialize($sessionData);
        try {
            $session = $this->http->readPostResult('/session/', $entity, $this->params)
                ->getEntity();
        } catch (Exception $exception) {
            if ($exception->getCode() == 404) {
                $session = null;
            }
            throw $exception;
        }

        return (null !== $session);
    }

    public function destroy($sessionId): bool
    {
        $result = $this->http->readDeleteResult('/session/' . $this->sessionName . '/' . $sessionId . '/');
        return ($result) ? true : false;
    }

    /**
     * @SuppressWarnings(UnusedFormalParameter)
     * @SuppressWarnings(ShortMethodName)
     * @codeCoverageIgnore
     */
    public function gc($maxlifetime): int
    {
        /*
         * $compareTs = time() - $maxlifetime;
         * $query = '
         * DELETE FROM
         * sessiondata
         * WHERE
         * UNIX_TIMESTAMP(`ts`) < ? AND
         * sessionname=?
         * ';
         * $statement = $this->getWriter()->prepare($query);
         * return $statement->execute(array(
         * $compareTs,
         * $this->sessionName
         * ));
         */
        return 1;
    }
}
