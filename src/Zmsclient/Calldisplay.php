<?php
namespace BO\Zmsclient;

use BO\Slim\Request;

class Calldisplay
{
    const COOKIE_NAME = 'calldisplayId';

    /**
     * @SuppressWarnings(Superglobals)
     */
    public static function setCookieValue($value, Request $request): void
    {
        $_COOKIE[self::COOKIE_NAME] = $value;
        if (!headers_sent()) {
            setcookie(
                self::COOKIE_NAME,
                $value,
                [
                    'expires'  => time() + (60*60*24*365*10),
                    'path'     => $request->getBasePath(),
                    'domain'   => null,
                    'secure'   => false,
                    'samesite' => 'Lax',
                ]
            );
        }
    }

    /**
     * @SuppressWarnings(Superglobals)
     * @return string|false
     */
    public static function getCookieValue()
    {
        if (array_key_exists(self::COOKIE_NAME, $_COOKIE)) {
            return $_COOKIE[self::COOKIE_NAME];
        }
        return false;
    }
}
